define([
  'jquery',
  'underscore',
  'backbone',
  'models/hall/place'
], function($, _, Backbone, Place){
  var HallPlaces = Backbone.Collection.extend({
    model: Place,
    url: '/inspection',
    initialize: function(){
        // console.log('inspectionPropertyCollection');
        // console.log('model',new this.model());
    },
   
  
  });
  return HallPlaces;
});
define([
  'jquery',
  'underscore',
  'backbone',
  'models/hall/selectPlace'
], function($, _, Backbone, selectPlace){
  var selectPlaces = Backbone.Collection.extend({
    model: selectPlace,
    initialize: function(){
    },
    getBySeatRow: function(seat,row){
      return this.filter(function(val){
        return (val.get('seat') == seat && val.get('row') == row);
      });
    }
   
  
  });
  return selectPlaces;
});
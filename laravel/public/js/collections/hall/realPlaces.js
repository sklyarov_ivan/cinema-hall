define([
  'jquery',
  'underscore',
  'backbone',
  'models/hall/realPlace'
], function($, _, Backbone, realPlace){
  var HallPlaces = Backbone.Collection.extend({
    model: realPlace,
    initialize: function(){
    },
   
  
  });
  return HallPlaces;
});
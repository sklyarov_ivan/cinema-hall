define([
  'jquery',
  'underscore',
  'backbone',
], function($, _, Backbone){
  var HallPlace = Backbone.Model.extend({
    defaults: {
      id: '',
      row: '',
      seat: '',
      posX: '',
      posY: '',
      width: ''
    },
    initialize: function(){
    },
  });
    
  return HallPlace;
});
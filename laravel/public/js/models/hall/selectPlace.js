define([
  'jquery',
  'underscore',
  'backbone',
], function($, _, Backbone){
  var selectPlace = Backbone.Model.extend({
    defaults: {
      id: '',
      placeid: '',
      seat: '',
      row: '',
    },
    initialize: function(){
    },
  });
    
  return selectPlace;
});
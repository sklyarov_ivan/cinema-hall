define([
  'jquery',
  'underscore',
  'backbone',
  // 'backbone_validate'
], function($, _, Backbone){
  var HallPlace = Backbone.Model.extend({
    // urlRoot: '/agents',
    defaults: {
      id: '',
      hallid: '',
      place: '',
      x_pos: '',
      y_pos: '',
      row: '',
      status: ''
    },
    initialize: function(){
    },
    // validate: function(attrs, options){
    //     var errors = {};
    //         if($.trim(attrs.agent_name).length<1){
    //           errors.agent_name = 'agency name must be longer than 1 simbols';
    //         }
    //         if($.trim(attrs.pm_name).length<1){
    //           errors.pm_name = 'Pm name must be longer than 1 simbols';
    //         }
    //         if($.trim(attrs.agent_phone).length!=11 || !isFinite(attrs.agent_phone)){
    //           errors.agent_phone = 'phone must have only 11 muneric simbols';
    //         }
    //       var re = /(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    //       if(!re.test(attrs.agent_email))
    //           errors.agent_email = 'not valid email';
    //         //password_required param (validate on differen forms)
    //       // if (attrs.password_required){
    //           if (attrs.agent_password == attrs.agent_password_confirm && attrs.agent_password.length>=6){
    //           } else {
    //             errors.agent_password ='not equal password and confirm password or lower 6';
    //           }
    //       // }
    //     return (!_.isEmpty(errors))?errors:'';
    // },

    
  });
    
  return HallPlace;
});
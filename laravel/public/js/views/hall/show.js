define([
    'jquery',
    'underscore',
    'backbone',
    'enquire',
    'text!templates/hall-show.html',
    ],function($,_,Backbone,enquire, HallShow){
        var HallShow = Backbone.View.extend({
            template: _.template(HallShow),
            $container: $('#content-container'),
            render: function(){
                var self=this;
                this.setElement(this.template({places:this.collection, factor:this.factor}));
                this.$container.html(this.$el);
                $('.seat-origin-place').bind('click', function(event){
                    self.placeListener(event,self)
                })
                this.mediaQueries();
            },
            initialize: function(data){
                this.collection = data.collection;
                this.selectCollection = data.selectCollection;
                this.factor = {
                    fx: 50,
                    fy: 50,
                    fw: 50
                };
                this.render();
            },
            mediaQueries: function(){
                    console.log(enquire);
                      var seat = $('.seat-origin-place');
                    enquire.register("screen and (max-width:600px)",function(){
                        _.each($('.seat-origin-place'),function(i,e){
                            var elem = $(i);
                              var originWidth = elem.css('width');
                              var originHeight = elem.css('height');
                              var originTop = elem.css('top');
                              var originLeft = elem.css('left');

                            elem.css({'width':originWidth.substring(0,originWidth.length-2)/2+'px',
                                'height':originHeight.substring(0,originHeight.length-2 )/2+'px',
                                'left':originLeft.substring(0,originLeft.length-2)/2+'px',
                                'top':originTop.substring(0,originTop.length-2)/2+'px',
                            });
                        })
                      // console.log('resize');
                      // seat.css({'width':originWidth.substring(0,originWidth.length-2)/2+'px',
                      //           'height':originHeight.substring(0,originHeight.length-2 )/2+'px'});

                    });
                     enquire.register("screen and (min-width:600px)",function(){
                      console.log('unresize');

                            // seat.css({'width':originWidth,'height':originHeight});
                     });
            },
            placeListener: function(event,self){
                var row,seat,placeid;
                var element = $(event.target);
                if (element.is('span'))
                    element = element.parent();
                row = element.attr('data-row');
                seat = element.attr('data-seat');
                if (!element.attr('data-status')){

                    if (!element.attr('data-selected')){
                        element.addClass('place-selected').attr({"data-selected":'on'});
                        self.selectCollection.add({row: element.attr('data-row'),
                                                    seat:element.attr('data-seat'),
                                                    placeid:element.attr('data-placeid')});
                    }
                    else{
                        self.selectCollection.remove(self.selectCollection.getBySeatRow(seat,row));
                        element.removeClass('place-selected').removeAttr("data-selected");
                        
                    }
                }
                    console.log('collection',self.selectCollection.toJSON());
            }
            
        });


        return HallShow;
    })
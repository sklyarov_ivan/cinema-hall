define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/hall-create.html',
    ],function($,_,Backbone, HallCreate){
        var HallCreate = Backbone.View.extend({
            template: _.template(HallCreate),
            $container: $('#content-container'),
            render: function(){
            },
            initialize: function(data){
            },
        });


        return HallCreate;
    })
// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
  paths: {
    jquery: 'libs/jquery/jquery.min',
    underscore: 'libs/underscore/underscore.min',
    backbone: 'libs/backbone/backbone.min',
    bootstrap: 'bootstrap-3.0.0/dist/js/bootstrap.min',
    text: 'libs/require-plugins/text',
    async: 'libs/require-plugins/async',
    enquire: 'libs/enquire/enquire.min'
    // font: 'lib/require-plugins/font',
    // goog: 'lib/require-plugins/goog',
    // image: 'lib/require-plugins/image',
    // json: 'lib/require-plugins/json',
    // noext: 'lib/require-plugins/noext',
    // mdown: 'lib/require-plugins/mdown',
    // propertyParser : 'lib/require-plugins/propertyParser',
    // markdownConverter : 'lib/Markdown.Converter'
  },
    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        underscore: {
            deps: ["jquery"],
            exports: "_"
        },
        bootstrap: {
          deps: ['jquery']
        }
    }

});

require([

  // Load our app module and pass it to our definition function
  'app',
], function(App){
  // The "app" dependency is passed in as "App"
  App.initialize();
});
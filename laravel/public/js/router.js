// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'bootstrap',
  // 'views/hall/create',
  'views/viewDefault',
  'views/hall/show',
  'collections/hall/places',
  'collections/hall/realPlaces',
  'collections/hall/selectCollection',

  // 'bootstrap-3.0.0/dist/js/bootstrap.min',
  'bootstrap-3.0.0/assets/js/holder'
  // 'views/projects/list',
  // 'views/users/list'
], function($, _, Backbone, bootstrap, viewDefault,showHall,placesCollection,realPlaceCollection,SelectCollection){
  Backbone.View.prototype.leave = function(){
    this.off();
    this.$el.remove();
    this.remove();
    this.unbind();
    if(this.onLeave){
      this.onLeave();
    }
  };

  var AppRouter = Backbone.Router.extend({
    routes: {
      // Define some URL routes
      'createHall': 'createHall',
      'showHall': 'showHall',

      // Default
      '*actions': 'defaultAction'
    },
    showHall: function(){
      var selectcollection = new SelectCollection();
      var placecollection = new realPlaceCollection(this.placeTestJson);
      var showhall = new showHall({collection:placecollection,selectCollection:selectcollection});

      this.swap(showhall);
    },
    initialize: function(){

      this.placeTestJson = [
      { "row": 1, "seat": 1, "posX": 1, "posY": 0.7, "width": 1, "height": 1 },
      { "row": 1, "seat": 2, "posX": 2, "posY": 0.8, "width": 1, "height": 1 },
      { "row": 1, "seat": 3, "posX": 3, "posY": 0.9, "width": 1, "height": 1 },
      { "row": 1, "seat": 4, "posX": 4, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 5, "posX": 5, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 6, "posX": 6, "posY": 1, "width": 1, "height": 1, "status": 1 },
      { "row": 1, "seat": 7, "posX": 7, "posY": 1, "width": 1, "height": 1, "status": 1 },
      { "row": 1, "seat": 8, "posX": 8, "posY": 1, "width": 1, "height": 1, "status": 1 },
      { "row": 1, "seat": 9, "posX": 9, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 10, "posX": 10, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 11, "posX": 11, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 12, "posX": 12, "posY": 1, "width": 1, "height": 1 },
      { "row": 1, "seat": 13, "posX": 13, "posY": 0.9, "width": 1, "height": 1 },
      { "row": 1, "seat": 14, "posX": 14, "posY": 0.8, "width": 1, "height": 1 },
      { "row": 1, "seat": 15, "posX": 15, "posY": 0.7, "width": 1, "height": 1 },

      { "row": 2, "seat": 2, "posX": 2, "posY": 1.8, "width": 1, "height": 1 },
      { "row": 2, "seat": 3, "posX": 3, "posY": 1.9, "width": 1, "height": 1 },
      { "row": 2, "seat": 4, "posX": 4, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 5, "posX": 5, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 6, "posX": 6, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 7, "posX": 7, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 8, "posX": 8, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 9, "posX": 9, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 10, "posX": 10, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 11, "posX": 11, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 12, "posX": 12, "posY": 2, "width": 1, "height": 1 },
      { "row": 2, "seat": 13, "posX": 13, "posY": 1.9, "width": 1, "height": 1 },
      { "row": 2, "seat": 14, "posX": 14, "posY": 1.8, "width": 1, "height": 1 },
      { "row": 2, "seat": 15, "posX": 15, "posY": 1.7, "width": 1, "height": 1 },

      { id:"30", "row": 3, "seat": 1, "posX": 1, "posY": 3, "width": 1, "height": 1 },
      { id:"31", "row": 3, "seat": 2, "posX": 2, "posY": 3, "width": 1, "height": 1 },
      { id:"32", "row": 3, "seat": 3, "posX": 3, "posY": 3, "width": 1, "height": 1 },
      { id:"33", "row": 3, "seat": 4, "posX": 4, "posY": 3, "width": 1, "height": 1 },
      { id:"34", "row": 3, "seat": 5, "posX": 5, "posY": 3, "width": 1, "height": 1 },
      { id:"35", "row": 3, "seat": 6, "posX": 6, "posY": 3, "width": 1, "height": 1 },
      { id:"36", "row": 3, "seat": 7, "posX": 7, "posY": 3, "width": 1, "height": 1 },
      { id:"37", "row": 3, "seat": 8, "posX": 8, "posY": 3, "width": 1, "height": 1 },
      { id:"38", "row": 3, "seat": 9, "posX": 9, "posY": 3, "width": 1, "height": 1 },
      { id:"39", "row": 3, "seat": 10, "posX": 10, "posY": 3, "width": 1, "height": 1 },
      { id:"40", "row": 3, "seat": 11, "posX": 11, "posY": 3, "width": 1, "height": 1 },
      { id:"41", "row": 3, "seat": 12, "posX": 12, "posY": 3, "width": 1, "height": 1 },
      { id:"42", "row": 3, "seat": 13, "posX": 13, "posY": 3, "width": 1, "height": 1 },
      { id:"43", "row": 3, "seat": 14, "posX": 14, "posY": 3, "width": 1, "height": 1 },
      { id:"44", "row": 3, "seat": 15, "posX": 15, "posY": 3, "width": 1, "height": 1 },

      { id:"45", "row": 4, "seat": 1, "posX": 1, "posY": 4, "width": 1, "height": 1 },
      { id:"46", "row": 4, "seat": 2, "posX": 2, "posY": 4, "width": 1, "height": 1 },
      { id:"47", "row": 4, "seat": 3, "posX": 3, "posY": 4, "width": 1, "height": 1 },
      { id:"48", "row": 4, "seat": 4, "posX": 4, "posY": 4, "width": 1, "height": 1 },
      { id:"49", "row": 4, "seat": 5, "posX": 5, "posY": 4, "width": 1, "height": 1 },
      { id:"50", "row": 4, "seat": 6, "posX": 6, "posY": 4, "width": 1, "height": 1 },
      { id:"51", "row": 4, "seat": 7, "posX": 7, "posY": 4, "width": 1, "height": 1 },
      { id:"52", "row": 4, "seat": 8, "posX": 8, "posY": 4, "width": 1, "height": 1 },
      { id:"53", "row": 4, "seat": 9, "posX": 9, "posY": 4, "width": 1, "height": 1 },
      { id:"54", "row": 4, "seat": 10, "posX": 10, "posY": 4, "width": 1, "height": 1 },
      { id:"55", "row": 4, "seat": 11, "posX": 11, "posY": 4, "width": 1, "height": 1 },
      { id:"56", "row": 4, "seat": 12, "posX": 12, "posY": 4, "width": 1, "height": 1 },
      { id:"57", "row": 4, "seat": 13, "posX": 13, "posY": 4, "width": 1, "height": 1 },
      { id:"58", "row": 4, "seat": 14, "posX": 14, "posY": 4, "width": 1, "height": 1 },
      { id:"59", "row": 4, "seat": 15, "posX": 15, "posY": 4, "width": 1, "height": 1 },

      { id:"60", "row": 5, "seat": 1, "posX": 1, "posY": 5, "width": 1, "height": 1 },
      { id:"61", "row": 5, "seat": 2, "posX": 2, "posY": 5, "width": 1, "height": 1 },
      { id:"62", "row": 5, "seat": 3, "posX": 3, "posY": 5, "width": 1, "height": 1 },
      { id:"63", "row": 5, "seat": 4, "posX": 4, "posY": 5, "width": 1, "height": 1 },
      { id:"64", "row": 5, "seat": 5, "posX": 5, "posY": 5, "width": 1, "height": 1 },
      { id:"65", "row": 5, "seat": 6, "posX": 6, "posY": 5, "width": 1, "height": 1 },
      { id:"66", "row": 5, "seat": 7, "posX": 7, "posY": 5, "width": 1, "height": 1 },
      { id:"67", "row": 5, "seat": 8, "posX": 8, "posY": 5, "width": 1, "height": 1 },
      { id:"68", "row": 5, "seat": 9, "posX": 9, "posY": 5, "width": 1, "height": 1 },
      { id:"69", "row": 5, "seat": 10, "posX": 10, "posY": 5, "width": 1, "height": 1 },
      { id:"70", "row": 5, "seat": 11, "posX": 11, "posY": 5, "width": 1, "height": 1 },
      { id:"71", "row": 5, "seat": 12, "posX": 12, "posY": 5, "width": 1, "height": 1 },
      { id:"72", "row": 5, "seat": 13, "posX": 13, "posY": 5, "width": 1, "height": 1 },
      { id:"73", "row": 5, "seat": 14, "posX": 14, "posY": 5, "width": 1, "height": 1 },
      { id:"74", "row": 5, "seat": 15, "posX": 15, "posY": 5, "width": 1, "height": 1 },

      { id:"75", "row": 6, "seat": 1, "posX": 1, "posY": 7, "width": 1, "height": 1 },
      { id:"76", "row": 6, "seat": 2, "posX": 2, "posY": 7, "width": 1, "height": 1 },
      { id:"77", "row": 6, "seat": 3, "posX": 3, "posY": 7, "width": 1, "height": 1 },
      { id:"78", "row": 6, "seat": 4, "posX": 7, "posY": 7, "width": 1, "height": 1 },
      { id:"79", "row": 6, "seat": 5, "posX": 8, "posY": 7, "width": 1, "height": 1 },
      { id:"80", "row": 6, "seat": 6, "posX": 9, "posY": 7, "width": 1, "height": 1 },
      { id:"81", "row": 6, "seat": 7, "posX": 10, "posY": 7, "width": 1, "height": 1 },
      { id:"82", "row": 6, "seat": 8, "posX": 11, "posY": 7, "width": 1, "height": 1 },
      { id:"83", "row": 6, "seat": 9, "posX": 12, "posY": 7, "width": 1, "height": 1 },
      { id:"84", "row": 6, "seat": 10, "posX": 13, "posY": 7, "width": 1, "height": 1 },
      { id:"85", "row": 6, "seat": 11, "posX": 14, "posY": 7, "width": 1, "height": 1 },
      { id:"86", "row": 6, "seat": 12, "posX": 15, "posY": 7, "width": 1, "height": 1 },

      { id:"87", "row": 7, "seat": 1, "posX": 1, "posY": 8, "width": 1, "height": 1 },
      { id:"88", "row": 7, "seat": 2, "posX": 2, "posY": 8, "width": 1, "height": 1 },
      { id:"89", "row": 7, "seat": 3, "posX": 3, "posY": 8, "width": 1, "height": 1 },
      { id:"90", "row": 7, "seat": 4, "posX": 7, "posY": 8, "width": 1, "height": 1 },
      { id:"91", "row": 7, "seat": 5, "posX": 8, "posY": 8, "width": 1, "height": 1 },
      { id:"92", "row": 7, "seat": 6, "posX": 9, "posY": 8, "width": 1, "height": 1 },
      { id:"93", "row": 7, "seat": 7, "posX": 10, "posY": 8, "width": 1, "height": 1 },
      { id:"94", "row": 7, "seat": 8, "posX": 11, "posY": 8, "width": 1, "height": 1 },
      { id:"95", "row": 7, "seat": 9, "posX": 12, "posY": 8, "width": 1, "height": 1 },
      { id:"96", "row": 7, "seat": 10, "posX": 13, "posY": 8, "width": 1, "height": 1 },
      { id:"97", "row": 7, "seat": 11, "posX": 14, "posY": 8, "width": 1, "height": 1 },
      { id:"98", "row": 7, "seat": 12, "posX": 15, "posY": 8, "width": 1, "height": 1 },

      { id:"99", "row": 8, "seat": 1, "posX": 1, "posY": 9, "width": 1, "height": 1 },
      { id:"100", "row": 8, "seat": 2, "posX": 2, "posY": 9, "width": 1, "height": 1 },
      { id:"101", "row": 8, "seat": 3, "posX": 3, "posY": 9, "width": 1, "height": 1 },
      { id:"102", "row": 8, "seat": 4, "posX": 9, "posY": 9, "width": 1, "height": 1 },
      { id:"103", "row": 8, "seat": 5, "posX": 10, "posY": 9, "width": 1, "height": 1 },
      { id:"104", "row": 8, "seat": 6, "posX": 11, "posY": 9, "width": 1, "height": 1 },
      { id:"105", "row": 8, "seat": 7, "posX": 12, "posY": 9, "width": 1, "height": 1 },
      { id:"106", "row": 8, "seat": 8, "posX": 13, "posY": 9, "width": 1, "height": 1 },
      { id:"107", "row": 8, "seat": 9, "posX": 14, "posY": 9, "width": 1, "height": 1 },
      { id:"108", "row": 8, "seat": 10, "posX": 15, "posY": 9, "width": 1, "height": 1 },

      { id:"109", "row": 9, "seat": 1, "posX": 1, "posY": 10, "width": "1.5", "height": 1 },
      { id:"110", "row": 9, "seat": 2, "posX": 2.5, "posY": 10, "width": "1.5", "height": 1 },
      { id:"111", "row": 9, "seat": 3, "posX": 4, "posY": 10, "width": "1.5", "height": 1 },
      { id:"112", "row": 9, "seat": 4, "posX": 5.5, "posY": 10, "width": "1.5", "height": 1 },
      { id:"113", "row": 9, "seat": 5, "posX": 7, "posY": 10, "width": "1.5", "height": 1 },
      { id:"114", "row": 9, "seat": 6, "posX": 8.5, "posY": 10, "width": "1.5", "height": 1 },
      { id:"115", "row": 9, "seat": 7, "posX": 10, "posY": 10, "width": "1.5", "height": 1 },
      { id:"116", "row": 9, "seat": 8, "posX": 11.5, "posY": 10, "width": "1.5", "height": 1 },
      { id:"117", "row": 9, "seat": 9, "posX": 13, "posY": 10, "width": "1.5", "height": 1 },
      { id:"118", "row": 9, "seat": 10, "posX": 14.5, "posY": 10, "width": "1.5", "height": 1 },

      { id:"119", "row": 10, "seat": 1, "posX": 1, "posY": 11, "width": "1.5", "height": 1 },
      { id:"120", "row": 10, "seat": 2, "posX": 2.5, "posY": 11, "width": "1.5", "height": 1 },
      { id:"121", "row": 10, "seat": 3, "posX": 4, "posY": 11, "width": "1.5", "height": 1 },
      { id:"122", "row": 10, "seat": 4, "posX": 5.5, "posY": 11, "width": "1.5", "height": 1 },
      { id:"123", "row": 10, "seat": 5, "posX": 7, "posY": 11, "width": "1.5", "height": 1 },
      { id:"124", "row": 10, "seat": 6, "posX": 8.5, "posY": 11, "width": "1.5", "height": 1 },
      { id:"125", "row": 10, "seat": 7, "posX": 10, "posY": 11, "width": "1.5", "height": 1 },
      { id:"126", "row": 10, "seat": 8, "posX": 11.5, "posY": 11, "width": "1.5", "height": 1 },
      { id:"127", "row": 10, "seat": 9, "posX": 13, "posY": 11, "width": "1.5", "height": 1 },
      { id:"128", "row": 10, "seat": 10, "posX": 14.5, "posY": 11, "width": "1.5", "height": 1 }
    ] ;

    },
    defaultAction: function(){
      // $('.navbar-nav li').removeClass('active').find('a[href=#'+Backbone.history.fragment+']').parent().addClass('active');
      // var defaultview = new viewDefault();
      // this.swap(defaultview);
      this.showHall();
    },
    swap: function(view){
      $('.navbar-nav li').removeClass('active').find('a[href=#'+Backbone.history.fragment+']').parent().addClass('active');
      if (this.currentView){
        this.currentView.leave();
      }
      this.currentView = view;
    }
  });

  var initialize = function(){
    var app_router = new AppRouter;
    Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});
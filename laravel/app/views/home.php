<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>Theme Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="laravel/public/js/bootstrap-3.0.0/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="laravel/public/js/bootstrap-3.0.0/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="laravel/public/css/style.css" rel="stylesheet">
    <link href="laravel/public/css/main.css" rel="stylesheet">


    <!--<script type="text/javascript" src="laravel/public/js/scr/header.js"></script>-->
    <!--<script type="text/javascript" src="laravel/public/js/libs/drug-and-drop/redips-drag-min.js"></script>-->
    <!--<script type="text/javascript" src="laravel/public/js/scr/script.js"></script>-->
    <!-- load jQuery -->
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>-->
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>-->
    <!-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css"/> -->


    <!-- Custom styles for this template -->
    <!-- <link href="theme.css" rel="stylesheet"> -->
   <script data-main="laravel/public/js/main" src="laravel/public/js/libs/require/require.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  <style id="holderjs-style" type="text/css">.holderjs-fluid {font-size:16px;font-weight:bold;text-align:center;font-family:sans-serif;margin:0}</style></head>

  <body style="">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bootstrap theme</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <!-- <li><a href="#createHall">create Hall</a></li> -->
            <li><a href="#showHall">show Hall</a></li>

            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div id="content-container">
      


    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="laravel/public/bootstrap-3.0.0/assets/js/jquery.js"></script>-->
    <!--<script src="laravel/public/bootstrap-3.0.0/dist/js/bootstrap.min.js"></script>-->
    <!--<script src="laravel/public/bootstrap-3.0.0/assets/js/holder.js"></script>-->
  

</body></html>

